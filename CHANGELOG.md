# Changelog

All notable changes to this project will be documented in this file.
Issue numbers refer to [issues in Gitlab](https://gitlab.com/beli-sk/bash-kubetmp/-/issues).

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.5] - 2023-09-20
### Fixed
- use shell PID in tmp files and cleanup on exit #1
- `ns` command unexpectedly switching contexts #2
- error out on non-existent context #3
- fix autocomplete failing on other than the last word #4

## [0.0.4] - 2021-10-15
### Fixed
- safe file mode when creating temporary config
- fix completion of namespaces from different context
### Added
- cleanup of possible stale config on shell start

## [0.0.3] - 2021-09-30
### Added
- Changelog
- Readme

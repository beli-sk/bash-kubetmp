# kubetmp

Create temporary kubeconfig for current bash session using specified context
and namespace. This allows to work with different Kubernetes context and
namespace in different terminal windows.

Includes auto-completion for all arguments.


## Requirements

- bash (developed on bash 5.1 but _should_ be fairly backwards compatible)
- kubectl (v1.22, but again should be quite relaxed)


## Install

Place `bash_kubetmp` script somewhere in your home dir, for example as
`~/.bash_kubetmp` and source it when starting bash, for example by adding the
following line to your `.bashrc` (may be dependent on your setup)

```
[[ -f ~/.bash_kubetmp ]] && . ~/.bash_kubetmp
```

See variables near top of `bash_kubetmp` script for configuration options.

This makes `kubetmp` command available in your shell.


## Use

```
kubetmp [command] [args...]

commands:
  create temporary kubeconfig with specified context and/or namespace:
    context|ctx|c <context> [namespace]
    namespace|ns|n <namespace>
    (no command or args uses current context and namespace)
  special commands:
    exit|x -- reset and delete temporary config
    version -- show version information
    help -- show this text
```

for example

```
kubetmp c mycontext mynamespace
```


## License

```
Copyright 2021,2023 Michal Belica <https://beli.sk>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

